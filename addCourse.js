let inputName = document.querySelector("#name-input");
let inputDescription = document.querySelector("#description-input");
let inputPrice = document.querySelector("#price-input");
let token = localStorage.getItem('token');

document.querySelector('#form-addCourse').addEventListener('submit',(e) =>{


	e.preventDefault()


	console.log(inputName.value)
	console.log(inputDescription.value)
	console.log(inputPrice.value)

	fetch('http://localhost:4000/courses',{
		method: 'POST',
		headers: {
			'Content-Type': 'application/json',
			'Authorization': `Bearer ${token}`
		},
			body: JSON.stringify({
			name: inputName.value,
			description: inputDescription.value,
			price:  inputPrice.value
		})
	})
	.then(res => res.json())
	.then(data => {
		console.log(data);
	})

})