let profileName = document.querySelector('#profile-name')
let profileEmail = document.querySelector('#profile-email')
let profileNo = document.querySelector('#profile-mobile')
//get user details

// syntax: localStorage.getItem(<token>) - This will return the value of the key from our localStorage
let token = localStorage.getItem('token');
// console.log(token)
fetch('http://localhost:4000/users/getUserDetails',{

	headers: {
		'Authorization': `Bearer ${token}`
	
	}



})
	.then(res => res.json())
	.then(data =>{

			console.log(data)

	profileName.innerHTML = `Hello, ${data.firstName}, ${data.lastName}`
	profileEmail.innerHTML = `Email: ${data.email}`
	profileNo.innerHTML = `Mobile Number: ${data.mobileNo}`
	

	})